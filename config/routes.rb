Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/doc'
  mount Rswag::Api::Engine => '/doc'
  root to: redirect("/doc")
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
      post "/project", to: "project#create_project"
end
