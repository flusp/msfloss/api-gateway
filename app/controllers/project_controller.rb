require 'net/http'
require 'json'
require 'rest-client'

class ProjectController < ApplicationController

  def has_invalid_params?(data, params)
    params.each do |param|
      if(!data.key?(param))
        return true
      end
    end
    false
  end

  def generate_error(response)
    if(response)
      resp = {"code" => response.code, "message" => response.body }
    else
      resp = {"code" => 500, "message" => {"error" => "Internal server error"} }
    end
    resp
  end
    
  def create_git(data)
    if(has_invalid_params?(data, ["git_name", "git_url", "git_frequency"]))
      resp = {"code" => 405, "message" => {"error" => "Wrong params"}}
    else
      git_data = { :name => data[:git_name], :url => data[:git_url], :frequency => data[:git_frequency] }
      begin
        response = RestClient.post 'https://gret-server-staging.herokuapp.com/api/v1/repositories', git_data.to_json
        resp = {"code" => response.code, "message" => { "git_id" => JSON.parse(response.body)["rid"] } }
      rescue => e 
        response = e.response
        resp = generate_error(response)
      end
    end
    resp
  end

  def create_irc(data)
    if(has_invalid_params?(data, ["irc_channel", "irc_server"]))
      resp = {"code" => 405, "message" => {"error" => "Wrong params"}}
    else
      irc_data = { :channel => data[:irc_channel], :server => data[:irc_server] }
      if(data.key?("irc_password"))
        irc_data[:pw] = data[:irc_password]
      end
      begin
        response = RestClient.post 'http://msfloss.interscity.org/irc/register', irc_data.to_json
        resp = {"code" => response.code, "message" => { "irc_server" => data[:irc_server], "irc_channel" => data[:irc_channel] } }
      rescue => e 
        response = e.response
        resp = generate_error(response)
      end
    end
    resp
  end

  def create_email_list(data)
    if(has_invalid_params?(data, ["email_list_name"]))
      resp = {"code" => 405, "message" => {"error" => "Wrong params"}}
    else
      email_list_data = { :name => data[:email_list_name] }
      begin
        response = RestClient.post 'https://limitless-taiga-45801.herokuapp.com/api/v1/lists/subscribe',  email_list_data.to_json, {content_type: :json, accept: :json}
        resp = {"code" => response.code, "message" => { "email_list_id" => JSON.parse(response.body)["list_id"] } }
      rescue => e 
        response = e.response
        resp = generate_error(response)
      end
    end
    resp
  end

  def create_issue(data)
    if(has_invalid_params?(data, 
      ["issue_client", "issue_url","issue_project", "issue_user", 
       "issue_password", "issue_private_token"]))
       resp = {"code" => 405, "message" => {"error" => "Wrong params"}}
    else
      issue_data = {
        :client => data[:issue_client], 
        :url => data[:issue_url],
        :project => data[:issue_project],
        :user => data[:issue_user],
        :password => data[:issue_password],
        :private_token => data[:issue_private_token],
      }
      begin
        response = RestClient.post 'http://msfloss.interscity.org/issues/repository',  issue_data.to_json, {content_type: :json, accept: :json}
        resp = {"code" => response.code, "message" => { "issue_id" => JSON.parse(response.body)["_id"]["$oid"] } }
      rescue => e 
        response = e.response
        resp = generate_error(response)
      end
    end
    resp
  end

  def add_who_returned_error(response_message, bot)
    message = "Error creating "
    case bot
    when 0
      bot_name = "git"
    when 1
      bot_name = "IRC"
    when 2
      bot_name = "email list"
    when 3
      bot_name = "issue tracker"
    end
    message = message + bot_name + " bot! "
    if(response_message)
      message = message + response_message.to_s
    end
    message
    return message
  end

  def prepare_response(response_array)
    resp_data = {}
    i = 0
    response_array.each do |response|
      if(response["code"] != 200)
        response["message"]["error"] = add_who_returned_error(response["message"]["error"], i)
        response["message"].to_json
        return response
      end
      resp_data.merge!(response["message"])
      i = i + 1
    end
    response = { "code" => 200, "message" => resp_data.to_json }
  end

  def create_project
    data = params
    response_array = []
    response_array << create_git(data)
    response_array << create_irc(data)
    response_array << create_email_list(data)
    response_array << create_issue(data)
    resp = prepare_response(response_array)
    render :json => resp["message"], status: resp["code"]
  end
end
