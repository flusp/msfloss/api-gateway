require 'rails_helper'

RSpec.configure do |config|
  # Specify a root folder where Swagger JSON files are generated
  # NOTE: If you're using the rswag-api to serve API descriptions, you'll need
  # to ensure that it's configured to serve Swagger from the same folder
  config.swagger_root = Rails.root.to_s + '/swagger'

  # Define one or more Swagger documents and provide global metadata for each one
  # When you run the 'rswag:specs:to_swagger' rake task, the complete Swagger will
  # be generated at the provided relative path under swagger_root
  # By default, the operations defined in spec files are added to the first
  # document below. You can override this behavior by adding a swagger_doc tag to the
  # the root example_group in your specs, e.g. describe '...', swagger_doc: 'v2/swagger.json'
  config.swagger_docs = {
    'v1/swagger.json' => {
      swagger: '2.0',
      info: {
        title: 'API Gateway MsFloss',
        version: 'v1',
        description: "API Gateway MsFloss is an API REST that integrates MsFloss front-end and MsFloss bots.\n\n To see specific bots documentation see:\n - Git bot: https://gret-server-staging.herokuapp.com\n - IRC bot: http://msfloss.interscity.org/irc/\n - Email list bot: https://limitless-taiga-45801.herokuapp.com/\n - Issue tracker bot: http://msfloss.interscity.org/issues/doc\n"
      },
      paths: {}
    }
  }
end
