# spec/integration/pets_spec.rb
require 'swagger_helper'

describe 'Create Project API' do

  path '/project' do
    post 'Creates a project' do
      tags 'Project'
      consumes 'application/json'
      parameter name: :body,
                description: "Project info to instanciate bots.\n", 
                in: :body, 
                schema: {
                  type: :object,
                  properties: {
                    git_name: { type: :string },
                    git_url: { type: :string },
                    git_frequency: { type: :integer },
                    irc_channel: { type: :string },
                    irc_server: { type: :string },
                    irc_password: { type: :string},
                    email_list_name: { type: :string },
                    issue_client: { type: :string },
                    issue_url: { type: :string },
                    issue_project: { type: :string },
                    issue_user: { type: :string },
                    issue_password: { type: :string },
                    issue_private_token: { type: :string },
                  },
                  required: ['git_name', 'git_url', 'git_frequency', "irc_channel", "irc_server", "email_list_name", "issue_client", "issue_url", "issue_project", "issue_user", "issue_password", "issue_private_token"]
                }

      response '200', 'Project Created' do
        schema type: :object,
        properties: {
          git_id: { type: :integer },
          irc_server: { type: :string },
          irc_channel: { type: :string },
          email_list_id: { type: :integer },
          issue_id: { type: :string }
        }

        let(:body) { { "git_name": "Linux Kernel IIO", "git_url": "https://github.com/torvalds/linux/tree/master/drivers/iio", "git_frequency": 120, "irc_channel": "#msfloss", "irc_server": "irc.freenode.net", "email_list_name": "linux-iio", "issue_client": "bugzilla", "issue_url": "https://bugzilla.kernel.org", "issue_project": "ACPI", "issue_user": "agnei.silva@usp.br", "issue_password": "Agnei.Silva@2019", "issue_private_token": "wNygMmMkFYiTsLnBJbkx"} }
        run_test!
      end

      response '405', 'Invalid Params' do
        schema type: :object,
        properties: {
          error: { type: :string}
        }
        
        let(:body) { {"git_name": 'Linux Kernel IIO' } }
        run_test!
      end
    end
  end
end