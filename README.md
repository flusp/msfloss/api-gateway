# API Gateway MsFloss

API Gateway MsFloss is an API REST that integrates MsFloss front-end and
MsFloss bots.

## Documentation
To view the documentation, go to [https://api-gateway-msfloss-staging.herokuapp.com] or [http://msfloss.interscity.org/api-gateway/]

## Install Dependencies

Clone the repository and run

    $ bundle install

## Usage

You can run the API Gateway locally with:

    $ rails server

If you have `docker` and `docker-compose` installed, you can simply run

    $ docker-compose up
    
## Testing

To run our test suite, execute:

    $ bundle exec rspec
