FROM ubuntu

MAINTAINER msfloss_group@gmail.com

RUN \
    apt-get update && \
    DEBIAN_FRONTEND=noninteractive \
    apt-get install -qq \
    ruby \
    ruby-dev \
    cmake \
    zlib1g \
    zlib1g-dev \
    libxml2 \
    libxml2-dev \
    libpq-dev \
    tzdata \
    postgresql \
    postgresql-contrib
RUN mkdir /api-gateway
COPY . /api-gateway
WORKDIR /api-gateway
RUN gem update --system
RUN gem install bundler
RUN gem update --system
RUN bundle install

EXPOSE 4444

ENTRYPOINT ["/api-gateway/docker-compose-runner.sh"]
