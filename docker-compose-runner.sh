#!/bin/bash

cd /api-gateway

rm -f /api-gateway/tmp/pids/server.pid
rake db:create db:migrate
rails s -b 0.0.0.0 -p 4444
